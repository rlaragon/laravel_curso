<?php

namespace Database\Factories;

use App\Models\Curso;
use App\Models\Profesor;
use Illuminate\Database\Eloquent\Factories\Factory;

class CursoFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Curso::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        static $profesores;
        if (!isset($profesores)) {
            $profesores = Profesor::all();
        };

        return [
            'titulo' => $this->faker->sentence(mt_rand(2,4)),
            'descripcion' => $this->faker->paragraph(4),
            'profesor_id' => $profesores->random()->id,

        ];
    }
}
