<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
    	$cantidadEstudiantes = 500;
        // \App\Models\User::factory(10)->create();
    	\App\Models\Profesor::factory(50)->create();
    	\App\Models\Estudiante::factory($cantidadEstudiantes)->create();
		\App\Models\Curso::factory(40)->create()
			->each(function($curso) use ($cantidadEstudiantes) {
				$curso->estudiantes()
				 	->attach(
				 		array_rand(range(1, $cantidadEstudiantes)));
//				 		array_rand(range(1, $cantidadEstudiantes), 40));
			});
    }
}
