<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Document</title>
</head>
<body>
	<div>
		<table>
			<thead>
				<tr>
					<th>id</th>
					<th>nombre</th>
					<th>direccion</th>
					<th>telefono</th>
					<th>carrera</th>
				</tr>
			</thead>
			<tbody>
				@foreach ($valores as $valor)
				<tr>
					<td>{{ $valor->id }} </td>
					<td>{{ $valor->nombre }} </td>
					<td>{{ $valor->direccion }} </td>
					<td>{{ $valor->telefono }} </td>
					<td>{{ $valor->profesion }} </td>
				</tr>
				@endforeach
            </tbody>
		</table>
	</div>
</body>
</html>