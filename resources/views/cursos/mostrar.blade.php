<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Document</title>
</head>
<body>
	<div>
		<table>
			<thead>
				<tr>
				<th>id</th>
				<th>Profesor</th>
				<th>titulo</th>
				<th>descripcion</th>
				</tr>
			</thead>
			<tbody>
				@foreach ($cursos as $curso)
				<tr>
					<td>{{ $curso->id }} </td>
					<td>{{ $curso->profesor_id }} </td>
					<td>{{ $curso->titulo }} </td>
					<td>{{ $curso->descripcion }} </td>			
				</tr>
				@endforeach
            </tbody>
		</table>
	</div>
</body>
</html>