<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Document</title>
</head>
<body>
	<div>
		<table>
			<thead>
				<tr>
					<th>id</th>
					<th>nombre</th>
					<th>direccion</th>
					<th>telefono</th>
					<th>carrera</th>
				</tr>
			</thead>
			<tbody>
				@foreach ($estudiantes as $estudiante)
				<tr>
					<td>{{ $estudiante->id }} </td>
					<td>{{ $estudiante->nombre }} </td>
					<td>{{ $estudiante->direccion }} </td>
					<td>{{ $estudiante->telefono }} </td>
					<td>{{ $estudiante->carrera }} </td>
				</tr>
				@endforeach
            </tbody>
		</table>
	</div>
</body>
</html>