<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Profesores</title>
</head>
<body>
	<div>
		<table>
			<thead>
				<tr>
				<th>nombre</th>
				<th>direccion</th>
				<th>telefono</th>
				<th>profesion</th>
				</tr>
			</thead>
			<tbody>
				@foreach ($profesores as $profesor)
				<tr>
					<td>{{ $profesor->nombre }} </td>
					<td>{{ $profesor->direccion }} </td>
					<td>{{ $profesor->telefono }} </td>
					<td>{{ $profesor->profesion }} </td>
				</tr>
				@endforeach
            </tbody>
		</table>
	</div>
</body>
</html>