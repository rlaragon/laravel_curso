<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Document</title>
</head>
<body>


	<h1>Hola {{ $nombre }} {{ $apellido }}</h1>
	<div>
	{{-- @if (isset($opcional)) --}}
	@isset($opcional)
		La variable opcional vale {{ $opcional }}
	{{-- @endif --}}
	@endisset

	@for ($i=0; $i < sizeof($arreglo); $i++)
		{{ $arreglo[$i] }}
	@endfor

	@foreach ($arreglo as $indice => $valor)
		[{{ $indice }}] {{ $valor }}
	@endforeach
<br>
	@foreach ($arreglo as $valor)
		{{ $loop->iteration }} <br>
		{{ $loop->remaining }} <br>
		{{ $loop->first }} <br>
		{{ $loop->last }} <br>
		[{{ $loop->index }}] {{ $valor }} <br>
	@endforeach
<br>
	@foreach ($arreglo as $valor)
		@if ($loop->first) 
			Es el primero <br>
		@endif 
		{{ $loop->iteration }} <br>
		{{ $loop->remaining }} <br>
		@if ($loop->last) 		
			Es el último <br>
		@endif
		[{{ $loop->index }}] {{ $valor }} <br>
	@endforeach
	</div>
	<div>
		<a href=" {{ route('principal') }}" >Inicio</a><br>
		<a href=" {{ route('precios') }}">Precios</a><br>
		<a href=" {{ route('acerca') }}" >Acerca</a><br>

	</div>
</body>
</html>