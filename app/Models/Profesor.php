<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Curso;

class Profesor extends Model
{
    use HasFactory;
    protected $table = 'profesores';

    protected $fillable = [
    	'nombre','direccion','telefono','profesion'
    ];

    // define una relación 1:N
    public function cursos()
    {
    	return $this->hasMany('App\Models\Curso');
    }
}
