<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Profesor;
use App\Models\Estudiante;

class Curso extends Model
{
    use HasFactory;

    protected $table = 'cursos';

    protected $fillable = [
    	'titulo','descripcion','profesor_id'
    ];

    // Define una relación N:1
    public function profesor() 
    {
    	return $this->belongsTo('\App\Models\Profesor');
    }

    // Define una relación N:N
    public function estudiantes()
    {
    	return $this->belongsToMany('App\Models\Estudiante');
    }
}
