<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Curso;

class Estudiante extends Model
{
    use HasFactory;

    protected $table = 'estudiantes';

    protected $fillable = [
    	'nombre', 'direccion', 'telefono', 'carrera'
    ];

    // define una relación N:N
    public function cursos()
    {
    	return $this->belongsToMany('App\Models\Curso');
    }
}
