<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class NuevoControlador extends Controller
{
    // Combinamos parámetros de la ruta ($nombre) y de la petición
    // ?apellido=
    // uso con injection y la otra cabecera usa un helper
    // public function principal(Request $request, $nombre = null)
    // {
    // 	$valor = $request->apellido;
    // 	return "Hola Mundo $nombre $valor";
    // }

    /* Código con helper 
    public function principal($nombre = null)
    {
    	$valor = request()->apellido;
    	return "Hola Mundo $nombre $valor";
    }
    */

    public function principal(Request $request, $nombre = null)
    {
   		$arreglo = range(1,5);
   		$opcional = 'Mi opción';
    	//return view('main', ['nombre' => 'Miguel']);
    	return view('main')
    		->with( [
    				'nombre' => $nombre,
    				'apellido' => $request->apellido,
    				'arreglo' => $arreglo,
    				'opcional' => $opcional,
    				]);
    }

    public function acerca(Request $request) 
    {
    	return view('acerca')->with(['informacion' => 'Contáctenos en el teléfono 99999999']);
    }

    public function precios(Request $request) 
    {
    	return view('precios');
    }

}
