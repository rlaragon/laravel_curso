<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Profesor;
use App\Models\Curso;

class GenericController extends Controller
{
    public function mostrar($id = null)
    {
        $profesores = Profesor::all();
        return view('generic.mostrar')
         ->with(['valores' => $profesores]);
    }

    public function mostrarProfesorCurso($id = null)
    {
    	$curso = Curso::findOrFail($id); //devuelve una excepción 404
   	
    	$valor = $curso->profesor; //método del modelo
    	
        return view('generic.mostrarProfesor')
         ->with(['valor' => $valor]);
    }

}
