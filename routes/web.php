<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RegistrationController;
use App\Http\Controllers\NuevoControlador;
use App\Http\Controllers\EstudianteController;
use App\Http\Controllers\ProfesorController;
use App\Http\Controllers\CursoController;
use App\Http\Controllers\GenericController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Del curso
Route::get('/', 'NuevoControlador@principal');*/

/*Route::get('/{nombre?}',[NuevoControlador::class, 'principal']);*/
//Route::any('/{nombre?}',[NuevoControlador::class, 'principal']);

//Route::resource('estudiantes', EstudianteController::class);

Route::get('estudiantes/mostrar',[EstudianteController::class, 'mostrarTodos']);

Route::get('profesores/mostrar',[ProfesorController::class,'mostrarTodos']);

Route::get('cursos/mostrar',[CursoController::class,'mostrarTodos']);

Route::get('generic/mostrarProfesor/{id?}',[GenericController::class,'mostrarProfesorCurso']);



Route::get('/',[NuevoControlador::class, 'principal'])->name('principal');
Route::get('/acerca',[NuevoControlador::class, 'acerca'])->name('acerca');
Route::get('/precios',[NuevoControlador::class, 'precios'])->name('precios');